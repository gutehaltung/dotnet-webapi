using System.Runtime.Serialization;

namespace WebAppAPI.CustomException
{
    public class NotFoundServiceException : System.Exception
    {
        public NotFoundServiceException(string description)
        {
            Description = description;
        }

        protected NotFoundServiceException(SerializationInfo info, StreamingContext context, string description) : base(info, context)
        {
            Description = description;
        }

        public NotFoundServiceException(string? message, string description) : base(message)
        {
            Description = description;
        }

        public NotFoundServiceException(string? message, System.Exception? innerException, string description) : base(message, innerException)
        {
            Description = description;
        }

        public string Description { get; set; }
    }
}