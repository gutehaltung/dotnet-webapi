using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAppAPI.CustomException;
using WebAppAPI.Data;
using WebAppAPI.Helpers;
using WebAppAPI.Models;
using WebAppAPI.Models.DTO;
using WebAppAPI.Services.Interfaces;

namespace WebAppAPI.Services
{
    public class UserService : IUserService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public UserService(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<ServiceHelper<List<UserDto>>> GetOne(int id)
        {
            var users = await _dataContext.Users
                .Include(user => user.Deals)
                .FirstOrDefaultAsync(user => user.Id == id);
            return users != null
                ? new ServiceHelper<List<UserDto>>(result: new List<UserDto>() {_mapper.Map<UserDto>(users)})
                : new ServiceHelper<List<UserDto>>(isSuccess: false,
                    exception: new NotFoundServiceException("user not found"));
        }

        public Task<ServiceHelper<List<UserDto>>> GetSeveral(string name)
        {
            throw new NotImplementedException();
        }

        public Task<ServiceHelper<List<UserDto>>> AddUser(UserDto userDto)
        {
            throw new NotImplementedException();
        }

        public Task<ServiceHelper<List<UserDto>>> UpdateUser(UserDto userDto)
        {
            throw new NotImplementedException();
        }

        public Task<ServiceHelper<List<UserDto>>> RemoveUser(int userId)
        {
            throw new NotImplementedException();
        }
    }
}