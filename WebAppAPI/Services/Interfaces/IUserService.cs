using System.Collections.Generic;
using System.Threading.Tasks;
using WebAppAPI .Helpers;
using WebAppAPI .Models;
using WebAppAPI.Models.DTO;

namespace WebAppAPI .Services.Interfaces
{
    public interface IUserService
    {
        public Task<ServiceHelper<List<UserDto>>> GetOne(int id);
        public Task<ServiceHelper<List<UserDto>>> GetSeveral(string name);
        public Task<ServiceHelper<List<UserDto>>> AddUser(UserDto userDto);
        public Task<ServiceHelper<List<UserDto>>> UpdateUser(UserDto userDto);
        public Task<ServiceHelper<List<UserDto>>> RemoveUser(int userId);
    }
}