using System;
using System.Collections.Generic;
using WebAppAPI.Models;

namespace WebAppAPI .Helpers
{
    public record ServiceHelper<T>
    {
        public bool IsSuccess { get; }
        public T? Result { get; }
        public Exception? Exception { get; }

        public ServiceHelper(bool isSuccess)
        {
            IsSuccess = isSuccess;
        }

        public ServiceHelper(bool isSuccess, System.Exception? exception)
        {
            IsSuccess = isSuccess;
            Exception = exception;
        }

        public ServiceHelper(T? result)
        {
            IsSuccess = true;
            Result = result;
        }
    }
}