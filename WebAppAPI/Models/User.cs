using System.Collections.Generic;

namespace WebAppAPI.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PassCodeNumber { get; set; }
        public List<Deal> Deals { get; set; }
    }
}