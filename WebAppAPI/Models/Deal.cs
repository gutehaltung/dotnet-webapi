namespace WebAppAPI.Models
{
    public class Deal
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public User User { get; set; }
        public User Admin { get; set; }
    }
}