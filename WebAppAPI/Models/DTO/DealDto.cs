namespace WebAppAPI.Models.DTO
{
    public record DealDto(int Id,string Title, string Description, User User);
}