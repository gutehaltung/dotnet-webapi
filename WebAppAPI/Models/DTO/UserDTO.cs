using System.Collections.Generic;

namespace WebAppAPI.Models.DTO
{
    public record UserDto(int Id, string Name, List<Deal> Deals);
} 