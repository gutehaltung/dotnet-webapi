using FluentValidation;
using WebAppAPI.Models.DTO;

namespace WebAppAPI.Models.Validation
{
    public class UserValidation : AbstractValidator<UserDto>
    {
        public UserValidation()
        {
            RuleFor(userDto => userDto.Name).Length(3, 100).WithErrorCode("Error 503");
        }
    }
}