using AutoMapper;
using WebAppAPI.Models.DTO;

namespace WebAppAPI.Models
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<User, UserDto>()
                .ForMember(dto => dto.Id, cfg => cfg.MapFrom(user => user.Id))
                .ForMember(dto => dto.Name, cfg => cfg.MapFrom(user => user.Name))
                .ForMember(dto => dto.Deals, cfg => cfg.MapFrom(user => user.Deals));

            CreateMap<Deal, DealDto>()
                .ForMember(dto => dto.Id, cfg => cfg.MapFrom(deal => deal.Id))
                .ForMember(dto => dto.Title, cfg => cfg.MapFrom(deal => deal.Title))
                .ForMember(dto => dto.Description, cfg => cfg.MapFrom(deal => deal.Description))
                .ForMember(dto => dto.User, cfg => cfg.MapFrom(deal => deal.User));
        }
    }
}