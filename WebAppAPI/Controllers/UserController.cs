using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAppAPI.Models.DTO;
using WebAppAPI .Services.Interfaces;

namespace WebAppAPI .Controllers
{
    [ApiController]
    [Route(nameof(Models.User))]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService) => _userService = userService;
        
        [HttpGet("singe/{id:int}")]
        public async Task<IActionResult> GetSingleUser(int id)
        {
            var serviceHelper = await _userService.GetOne(id);
            return serviceHelper.IsSuccess ? Ok(serviceHelper.Result) : NotFound();
        }
        
        [HttpGet("multi/{name}")]
        public async Task<IActionResult> GetMultiUser(string name)
        {
            var serviceHelper = await _userService.GetSeveral(name);
            return serviceHelper.IsSuccess ? Ok(serviceHelper.Result) : NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> AddUser(UserDto userDto)
        {
            throw new NotImplementedException();
        }
    }
}